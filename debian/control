Source: motion
Section: video
Priority: optional
Maintainer: Ximin Luo <infinity0@debian.org>
Homepage: https://github.com/Motion-Project/motion
Build-Depends: debhelper (>= 13),
 debhelper-compat (= 13),
 dpkg-dev (>= 1.16),
 libavcodec-dev,
 libavdevice-dev,
 libavfilter-dev,
 libavformat-dev,
 libavutil-dev,
 libswscale-dev,
 libjpeg-dev,
 default-libmysqlclient-dev,
 libmicrohttpd-dev,
 libpq-dev,
 libsdl1.2-dev,
 libsqlite3-dev,
 libv4l-dev,
 zlib1g-dev,
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/infinity0/motion.git
Vcs-Browser: https://salsa.debian.org/infinity0/motion

Package: motion
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, lsb-base (>= 3.0-6), adduser
Recommends: ffmpeg
Suggests: default-mysql-client, postgresql-client
Description: V4L capture program supporting motion detection
 Motion is a program that monitors the video signal from
 one or more cameras and is able to detect if a significant
 part of the picture has changed. Or in other words, it can
 detect motion.
 .
 Motion is a command line based tool. It has no graphical
 user interface. Everything is setup either via the
 command line or via configuration files.
 .
 The output from motion can be:
    - jpg/ppm image files
    - mpeg/mp4/swf/flv/mov/ogg video sequences
 .
 Also, motion has its own minimalistic web server. Thus,
 you can access the webcam output from motion via a browser.
